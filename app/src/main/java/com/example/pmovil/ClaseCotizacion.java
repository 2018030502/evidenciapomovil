package com.example.pmovil;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Random;

public class ClaseCotizacion implements Serializable {
    private int folio, plazos;
    private String descripcion;
    private float valorAuto;
    private float porEnganche;

    public ClaseCotizacion(int plazos, String descripcion, float valorAuto, float porEnganche) {
        this.folio = generarFolio();
        this.plazos = plazos;
        this.descripcion = descripcion;
        this.valorAuto = valorAuto;
        this.porEnganche = porEnganche;
    }

    public ClaseCotizacion() {
        this.folio = generarFolio();
        this.plazos = 0;
        this.descripcion = "";
        this.valorAuto = 0.0f;
        this.porEnganche = 0.0f;
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public int getPlazos() {
        return plazos;
    }

    public void setPlazos(int plazos) {
        this.plazos = plazos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getValorAuto() {
        return valorAuto;
    }

    public void setValorAuto(float valorAuto) {
        this.valorAuto = valorAuto;
    }

    public float getPorEnganche() {
        return porEnganche;
    }

    public void setPorEnganche(float porEnganche) {
        this.porEnganche = porEnganche;
    }

    // Métodos de comportamiento
    public int generarFolio(){
        Random r = new Random();
        return r.nextInt(1000);
    }

    public float calcularPagoInicial(){
        return this.valorAuto * (this.porEnganche/100);
    }

    public float calcularPagoMensual(){
        return (this.valorAuto - this.calcularPagoInicial())/this.plazos;
    }
}
