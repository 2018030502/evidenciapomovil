package com.example.pmovil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.motion.utils.StopLogic;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class  DetalleCotizacion extends AppCompatActivity {
    private EditText txtUsuario1;
    private Button btnIngresar, btnRegresar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.detalle_cotizacion);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        txtUsuario1 = findViewById(R.id.txtUsuario1);
        btnIngresar = findViewById(R.id.btnIngresar);
        btnRegresar = findViewById(R.id.btnRegresar);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validacion();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), menuActivity.class);
                startActivity(intent);
            }
        });
    }



    public void validacion(){
        String userStr = txtUsuario1.getText().toString().trim();
        if (!userStr.isEmpty()){
            Cotizacion cotizacion = new Cotizacion(0, "", 0,0);
            Intent intent = new Intent(getApplicationContext(), cotizacionActivity.class);
            intent.putExtra("userStr",txtUsuario1.getText().toString());
            intent.putExtra("cotizacion", cotizacion);
            startActivity(intent);
        }else {
            Toast.makeText(this,"¡Por favor ingresa un usuario!",Toast.LENGTH_SHORT).show();
        }
    }
}