package com.example.pmovil;
import android.os.Bundle;

import android.os.Bundle;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class DetalleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);

        // Obtener la posición del elemento seleccionado
        int position = getIntent().getIntExtra("position", -1);
        if (position != -1) {
            // Determinar qué layout mostrar según la posición
            int layoutId = getLayoutForPosition(position);
            if (layoutId != 0) {
                setContentView(layoutId);
            } else {
                // Manejar el caso en que no haya un layout correspondiente
            }
        } else {
            // Manejar el caso en que no se haya pasado una posición válida
        }
    }

    // Método para obtener el layout correspondiente a una posición
    private int getLayoutForPosition(int position) {
        switch (position) {
            case 0:
                return R.layout.detalle_hola;
            case 1:
                return R.layout.detalle_calcular_imc;
            // Agrega más casos según la cantidad de imágenes
            case 2:
                return R.layout.detalle_conversion;
            case 3:
                return R.layout.detalle_cotizacion;
            case 4:
                return R.layout.detalle_moneda;
            case 5:
                return R.layout.detalle_spinner;
            default:
                return 0; // En caso de que no haya un layout correspondiente
        }
    }
}
