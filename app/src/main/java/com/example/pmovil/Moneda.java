package com.example.pmovil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import com.example.pmovil.R;

public class Moneda extends AppCompatActivity {

    private EditText txtCantidad;
    private Spinner spinnerMonedas;
    private TextView lblRes;
    private Button btnCalcular, btnLimpiar, btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalle_moneda);

        txtCantidad = findViewById(R.id.txtCantidad);
        spinnerMonedas = findViewById(R.id.spinnerMonedas);
        lblRes = findViewById(R.id.lblRes);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularConversion();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), menuActivity.class);
                startActivity(intent);
            }
        });
    }

    public void calcularConversion(){
        String cantidadStr = txtCantidad.getText().toString();
        if (cantidadStr.isEmpty()){
            Toast.makeText(this,"Por favor ingrese una cantidad", Toast.LENGTH_SHORT).show();
            return;
        }

        double pesos = Double.parseDouble(cantidadStr);
        String moneda = spinnerMonedas.getSelectedItem().toString();
        double resultado = 0;

        switch(moneda){
            case "Dólares Americanos":
                resultado = pesos * obtainTasaCambio("USD");
                break;
            case "Euros":
                resultado = pesos * obtainTasaCambio("EUR");
                break;
            case "Dólares Canadienses":
                resultado = pesos * obtainTasaCambio("CAD");
                break;
            case "Libras Esterlinas":
                resultado = pesos * obtainTasaCambio("GBP");
                break;
        }
        lblRes.setText(String.format("Total: %.2f %s", resultado, moneda));
    }

    public double obtainTasaCambio(String moneda){
        switch(moneda){
            case "USD":
                return 0.06;
            case "EUR":
                return 0.055;
            case "CAD":
                return 0.082;
            case "GBP":
                return 0.047;
            default:
                return 1;
        }
    }

    public void limpiarCampos(){
        txtCantidad.setText("");
        lblRes.setText("Total: ");
        spinnerMonedas.setSelection(0);
    }
}