package com.example.pmovil;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.pmovil.R;

public class Conversion extends AppCompatActivity {

    private EditText txtGrados;
    private RadioGroup rgBotones;
    private RadioButton rbFarenheit, rbCelsius;
    private TextView txtResultado;

    private Button btnCalcular, btnLimpiar, btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.detalle_conversion);
        iniciarComponentes();
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Validar

                int selectedId = rgBotones.getCheckedRadioButtonId();
                double grados, res = 0;

                grados = Float.parseFloat(txtGrados.getText().toString());

                if (txtGrados.getText().toString().matches("")) {
                    Toast.makeText(Conversion.this,
                            "Falló al capturar los datos", Toast.LENGTH_SHORT).show();
                }
                if (selectedId == -1) {
                    Toast.makeText(Conversion.this, "Por favor, selecciona una opción", Toast.LENGTH_SHORT).show();
                } else {
                    RadioButton selectedRadioButton = findViewById(selectedId);
                    String selectedText = selectedRadioButton.getText().toString();

                    // Aquí puedes agregar la lógica adicional basada en la opción seleccionada
                    if (selectedId == R.id.rbFarenheit) {
                        res = (grados * (1.8))+32;
                    } else if (selectedId == R.id.rbCelsius) {
                        res = (grados - 32) * (0.55556);
                    }
                    txtResultado.setText(String.valueOf(res));
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtResultado.setText("");
                txtGrados.setText("");
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes() {
        txtGrados = (EditText) findViewById(R.id.txtGrados);
        rgBotones = (RadioGroup) findViewById(R.id.rgBotones);
        rbFarenheit = (RadioButton) findViewById(R.id.rbFarenheit);
        rbCelsius = (RadioButton) findViewById(R.id.rbCelsius);
        txtResultado = (TextView) findViewById(R.id.txtResultado);

        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
    }
}